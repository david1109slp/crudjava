/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import accions.Iacciones;
import conexion.cconexion;
import java.sql.Connection;
import modelo.registro_datos;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;


/**
 *
 * @author driss
 */
public class registro_controller  implements Iacciones<registro_datos>{

    private   cconexion conexion = new cconexion();
    
    @Override
    public void crear(registro_datos object) {
        String sql = "INSERT INTO conductor (nombre_completo, cargo) VALUES (?, ?)";

        try (Connection connection = conexion.getConnection();
             PreparedStatement crear = connection.prepareStatement(sql)) {

            crear.setString(1, object.getNombre_completo());
            crear.setString(2, object.getCargo());

           crear.executeUpdate();
           
           
           JOptionPane.showMessageDialog(null, "Conductor registrado!");
           

           

        } catch (SQLException e) {
           JOptionPane.showMessageDialog(null, "Se presento un error, por favor corregir."); 
        }
    }

    @Override
    public registro_datos leer(int id) {
        String sql = "SELECT nombre_completo, cargo FROM conductor WHERE id_conductor = '" + id +"'";
        registro_datos conductorg = new registro_datos();
        
        try {
        Connection connection = conexion.getConnection();
         PreparedStatement consulta = connection.prepareStatement(sql);
         ResultSet  rs = consulta.executeQuery();
        
         if(rs.next()){
             conductorg.setNombre_completo(rs.getString("nombre_completo"));
             conductorg.setCargo(rs.getString("cargo"));
             
         } else{
             conductorg = new registro_datos();
              JOptionPane.showMessageDialog(null, "No se encontraron datos");
         }
        }catch(Exception e){
                 JOptionPane.showMessageDialog(null, "No se encontraron registros", " Error", JOptionPane.ERROR_MESSAGE);
                 System.out.println("error en la clase: "+this.getClass().getName());
        }
        return conductorg;
        
    }

    @Override
    public void actualizar(registro_datos objectup, int id) {
  
    String sql = "update conductor  set nombre_completo=?, cargo=? where id_conductor ='"+id+"'";
    
     try {
        Connection connection = conexion.getConnection();
         PreparedStatement actualizacion = connection.prepareStatement(sql);
         
         actualizacion.setString(1, objectup.getNombre_completo());
         actualizacion.setString(2, objectup.getCargo());
         
         actualizacion.executeUpdate();
         
                            JOptionPane.showMessageDialog(null, "Conductor modificado");
        
      
        }catch(SQLException e){
                 JOptionPane.showMessageDialog(null, "No se encontraron registros", " Error", JOptionPane.ERROR_MESSAGE);
                 System.out.println("error en la clase: "+this.getClass().getName());
        }
    
    
    }
    
  
    @Override
    public void eliminar(int id) {
        String sql = "DELETE FROM conductor WHERE id_conductor = '"+id+"'";
        
        try {
            Connection connection = conexion.getConnection();
              PreparedStatement eliminar = connection.prepareStatement(sql);
              int filasAfectadas = eliminar.executeUpdate();
              if(filasAfectadas > 0){
                   JOptionPane.showMessageDialog(null, "Conductor eliminado");
              }else{
              JOptionPane.showMessageDialog(null, "No se encontraron datos");
              }
        
        }catch(Exception e){    
            JOptionPane.showMessageDialog(null, "Error al Eliminar Conductor", " Error", JOptionPane.ERROR_MESSAGE);
        
        }
        
        
    }
    
   
    
    
    
}
