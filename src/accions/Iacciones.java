/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package accions;

/**
 *
 * @author driss
 */
public interface Iacciones<T>{
    void  crear(T object);
    T leer(int id);
    void actualizar(T objectup, int id);
    void eliminar(int id);
}
